package uk.co.nationalrail.rars.s3authutil;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.engine.discovery.predicates.IsNestedTestClass;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import uk.co.nationalrail.rars.s3authutil.api.S3AuthUtil;

public class Test {
	public static void main(String[] args) {
		try (InputStream is = Test.class.getResourceAsStream("/example_token.json");
				JsonReader reader = Json.createReader(is)) {
			JsonObject root = reader.readObject();
			S3AuthUtil.storeS3Token("rdg", root.getString("access_token"), root.getInt("expires_in"),
					root.getString("token_type"), !root.isNull("scope")? root.getString("access_token"): null, root.getString("refresh_token"), 4 * 60 * 60,
					false);

			String s3_access_token = S3AuthUtil.getActiveS3AccessToken("rdg");
			System.out.println("Got active token: " + s3_access_token);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
