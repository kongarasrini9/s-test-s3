package uk.co.nationalrail.rars.s3authutil;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.JsonValue;
import uk.co.nationalrail.rars.s3authutil.api.S3AuthUtil;

/**
 * Unit test for simple App.
 */
@SuppressWarnings("static-method")
public class AppTest {
	@BeforeAll
	public static void beforeAll() {
		try (InputStream is = Test.class.getResourceAsStream("/example_token.json");
				JsonReader reader = Json.createReader(is)) {
			JsonObject root = reader.readObject();
			String scope = null;
			JsonValue value = root.get("scope");
			if (value != null) {
				scope = value.toString();
			}
			S3AuthUtil.storeS3Token("rdg", root.getString("access_token"), root.getInt("expires_in"),
					root.getString("token_type"), scope, root.getString("refresh_token"), 4 * 60 * 60, false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testExpiredTokenAccess() {
		String s3_access_token = S3AuthUtil.getActiveS3AccessToken("rdg");
		System.out.println("Got active token: " + s3_access_token);
	}
	
	@Test
	public void testExpiredTokenAccessWithDelta() {
		String s3_access_token = S3AuthUtil.getActiveS3AccessToken("rdg", 5);
		System.out.println("Got active token: " + s3_access_token);
	}
	
}
