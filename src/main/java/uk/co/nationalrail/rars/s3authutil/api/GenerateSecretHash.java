package uk.co.nationalrail.rars.s3authutil.api;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateSecretHash {
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateSecretHash.class);
	public static void main(String[] args) {
		String client_id = "33bjrc9sdni5qgte1g6vchg7bb";
		String client_secret = "b3n8vpshnb09qigdubu4hd1itm4ftlj8ua4o04m5hf4u8rdme8e";
		String username = "dev-user1";

		String secret_hash = calculateSecretHash(client_id, client_secret, username);
		LOGGER.debug("secret_hash: " + secret_hash);
	}

	public static String calculateSecretHash(String userPoolClientId, String userPoolClientSecret, String userName) {
		final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

		SecretKeySpec signingKey = new SecretKeySpec(userPoolClientSecret.getBytes(StandardCharsets.UTF_8),
				HMAC_SHA256_ALGORITHM);
		try {
			Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
			mac.init(signingKey);
			mac.update(userName.getBytes(StandardCharsets.UTF_8));
			byte[] rawHmac = mac.doFinal(userPoolClientId.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(rawHmac);
		} catch (Exception e) {
			throw new RuntimeException("Error while calculating ");
		}
	}
}
