package uk.co.nationalrail.rars.s3authutil.api;

import java.io.StringReader;
import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;

/**
 *
 * Sqills tokens are received in the following JSON format:
 *
 * <pre>
 * {
 *     "access_token": "<header-b64-encoded>.<payload-b64-encoded>.<signature-b64-encoded>",
 *     "expires_in": 1800,
 *     "token_type": "bearer",
 *     "scope": null,
 *     "refresh_token": "<refresh-token-b64-encoded>"
 * }
 * </pre>
 *
 * The access token payload is a JSON object with the following key fields:
 * <ul>
 * <li>"iat" (issued at) - time at which the token was issued (seconds since
 * epoch time)</li>
 * <li>"exp" (expiry at) - time at which the token will expire (seconds since
 * epoch time), equal to iat + expires_in</li>
 * </ul>
 *
 * Tokens need to be refreshed whenever
 */
public class S3AuthUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(S3AuthUtil.class);

	private static final String JWT_IAT = "iat";
	private static final String JWT_EXP = "exp";
	private static long tokenDeltaExpirySec = 0;



	// Be conservative with token expiry times to avoid scenarios whereby the token
	// is valid when checked but is no longer valid when actually used.
	// TODO Get from an environment property and default to the below value
	static final long ACCESS_DELTA_SEC = 10;

	private static Map<String, S3Token> tokens;

	static {
		tokens = new ConcurrentHashMap<>();
	}

	private S3AuthUtil() {
	}

	/**
	 * Invoked by the System APIs to determine if they already have an active S3 token
	 * 
	 * @param agentId
	 * @return a JWT Token that can be used for S3 authorisation
	 */
	public static String getActiveS3AccessToken(String agentId) {
		S3Token token = tokens.get(agentId);
		if (token == null) {
			return null;
		}
		long access_timestamp;
		
		access_timestamp = System.currentTimeMillis() / 1000 + ACCESS_DELTA_SEC;

		 
		// Does the token need refreshing?
		long refresh_timestamp = token.getRefreshAtTimestamp();
		if (access_timestamp > refresh_timestamp) {
			LOGGER.debug("Token needs refreshing, delta={} seconds",
					Long.valueOf(access_timestamp - refresh_timestamp));
			return null;
		}

		// Has the token fully expired?
		long full_expiry_timestamp = token.getFullExpireAtTimestamp();
		if (access_timestamp > full_expiry_timestamp) {
			LOGGER.debug("Token has fully expired, delta = {} seconds",
					Long.valueOf(access_timestamp - full_expiry_timestamp));
			return null;
		}

		return token.getAccessToken();
	}


	/**
	 * Invoked by the System APIs to determine if they already have an active S3 token
	 * 
	 * @param agentId
	 * @param tokenDeltaExpirySec set token expiry value with delta setting below default 10 sec
	 * @return a JWT Token that can be used for S3 authorisation
	 */
	public static String getActiveS3AccessToken(String agentId, long tokenDeltaExpirySec) {
		S3Token token = tokens.get(agentId);
		
		if (token == null) {
			return null;
		}
		long access_timestamp;
		
		setTokenDeltaExpirySec(tokenDeltaExpirySec);
		
		if (tokenDeltaExpirySec == 0) {
			access_timestamp = System.currentTimeMillis() / 1000 + ACCESS_DELTA_SEC;

		} else {
			access_timestamp = System.currentTimeMillis() / 1000 + getTokenDeltaExpirySec();
		}
		 
		// Does the token need refreshing?
		long refresh_timestamp = token.getRefreshAtTimestamp();
		if (access_timestamp > refresh_timestamp) {
			LOGGER.debug("Token needs refreshing, delta={} seconds",
					Long.valueOf(access_timestamp - refresh_timestamp));
			return null;
		}

		// Has the token fully expired?
		long full_expiry_timestamp = token.getFullExpireAtTimestamp();
		if (access_timestamp > full_expiry_timestamp) {
			LOGGER.debug("Token has fully expired, delta = {} seconds",
					Long.valueOf(access_timestamp - full_expiry_timestamp));
			return null;
		}

		return token.getAccessToken();
	}
	// Invoked by the System APIs based on the response from the S3 Auth API
	public static void storeS3Token(String agentId, String accessToken, int expiresIn, String tokenType, String scope,
			String refreshToken, int refreshAtTimestamp, int fullExpireAtTimestamp) {
		tokens.put(agentId, new S3Token(agentId, accessToken, expiresIn, tokenType, scope, refreshToken,
				refreshAtTimestamp, fullExpireAtTimestamp));
	}

	// Invoked by the S3 Auth API only - it needs to know if there is currently a
	// token for this agent id. It subsequently determines if it is fully expired or
	// needs refreshing
	public static S3Token getS3Token(String agentId) {
		return tokens.get(agentId);
	}

	// Invoked by the S3 Auth API only
	public static S3Token storeS3Token(String agentId, String accessToken, int expiresIn, String tokenType,
			String scope, String refreshToken, int fullExpireIn, boolean refresh) {
		Objects.requireNonNull(agentId, "agentId must not be null");
		Objects.requireNonNull(accessToken, "accessToken must not be null");
		Objects.requireNonNull(tokenType, "tokenType must not be null");
		Objects.requireNonNull(refreshToken, "refreshToken must not be null");

		String[] jwt_parts = accessToken.split("\\.");
		if (jwt_parts.length != 3) {
			LOGGER.error("Error - JWT expected to contain 3 parts, found {}", Integer.valueOf(jwt_parts.length));
			return null;
		}

		String jwt_payload_json = new String(Base64.getDecoder().decode(jwt_parts[1]));

		S3Token token;
		try (JsonReader reader = Json.createReader(new StringReader(jwt_payload_json))) {
			// Calculate the token refresh and full expiry timestamp values
			JsonObject root = reader.readObject();

			long iat = root.getJsonNumber(JWT_IAT).longValue();
			long refresh_at = iat + expiresIn;
			// Double-check this matches with the JWT exp value
			long exp = root.getJsonNumber(JWT_EXP).longValue();
			if (exp != refresh_at) {
				LOGGER.error("Warning - calculated expires_at ({}) != JWT exp ({})", Long.valueOf(refresh_at),
						Long.valueOf(exp));
			}
			long full_expire_at = iat + fullExpireIn;

			// Update the existing token if this was the result of a token refresh so that
			// the full expiry timestamp is preserved
			token = tokens.get(agentId);
			if (refresh && token != null) {
				token.setAccessToken(accessToken);
				token.setRefreshAtTimestamp(refresh_at);
				token.setRefreshToken(refreshToken);
			} else {
				token = new S3Token(agentId, accessToken, expiresIn, tokenType, scope, refreshToken, refresh_at,
						full_expire_at);
				tokens.put(agentId, token);
			}
		}

		return token;
	}
	
	public static long getTokenDeltaExpirySec() {
		return tokenDeltaExpirySec;
	}

	private static void setTokenDeltaExpirySec(long tokenDeltaExpirySec) {
		S3AuthUtil.tokenDeltaExpirySec = tokenDeltaExpirySec;
	}
}
