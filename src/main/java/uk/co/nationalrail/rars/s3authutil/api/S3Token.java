package uk.co.nationalrail.rars.s3authutil.api;

public class S3Token {
	private String agentId;
	private String accessToken;
	private int expiresIn;
	private String tokenType;
	private String scope;
	private String refreshToken;
	private long refreshAtTimestamp;
	private long fullExpireAtTimestamp;

	public S3Token(String agentId, String accessToken, int expiresIn, String tokenType, String scope,
			String refreshToken, long refreshAtTimestamp, long fullExpireAtTimestamp) {
		this.agentId = agentId;
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.tokenType = tokenType;
		this.scope = scope;
		this.refreshToken = refreshToken;
		this.refreshAtTimestamp = refreshAtTimestamp;
		this.fullExpireAtTimestamp = fullExpireAtTimestamp;
	}

	public String getAgentId() {
		return agentId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public String getTokenType() {
		return tokenType;
	}

	public String getScope() {
		return scope;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public long getRefreshAtTimestamp() {
		return refreshAtTimestamp;
	}

	public void setRefreshAtTimestamp(long refreshAtTimestamp) {
		this.refreshAtTimestamp = refreshAtTimestamp;
	}

	public long getFullExpireAtTimestamp() {
		return fullExpireAtTimestamp;
	}

	public boolean getRefreshRequired() {
		return (System.currentTimeMillis() / 1000 + S3AuthUtil.ACCESS_DELTA_SEC) >= refreshAtTimestamp;
	}

	public boolean getFullyExpired() {
		return (System.currentTimeMillis() / 1000 + S3AuthUtil.ACCESS_DELTA_SEC) >= fullExpireAtTimestamp;
	}
}
