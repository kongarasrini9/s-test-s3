package uk.co.nationalrail.rars.s3authutil.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO Move the s3authutil-connector project as test cases, switch system.out to slf4j
public class KeystoreTest {
	private static final int AGENT_CERT_KEY_SIZE = 1024;
	private static final String RSA_ALGORITHM = "RSA";
	private static final String SIGNATURE_ALGORITHM = "SHA256WithRSA";

	private static final String AGENT_GRANT_TYPE = "https://com.sqills.s3.oauth.agent";

	private static final Logger LOGGER = LoggerFactory.getLogger(KeystoreTest.class);

	private static KeyStore KEY_STORE = null;
	private static PrivateKey CA_PRIVATE_KEY = null;
	private static X509Certificate CA_CERTIFICATE = null;

	// Self test
	public static void main(String[] args) {
		final String key_store_filename = "/keystore.p12";
		final String key_store_password = "rdg-dev";
		final String ca_alias = "rdg-dev-certauth-ca";
		final String ca_key_password = "rdg-dev";

		initialiseKeyStore(key_store_filename, key_store_password, ca_alias, ca_key_password);
		if (KEY_STORE == null) {
			System.out.println("Failed to load key store");
			return;
		}

		System.out.format("Is CA '%s' present in key store '%s'? %b%n", ca_alias, key_store_filename,
				Boolean.valueOf(isCertificateAuthorityPresent(ca_alias, ca_key_password)));

		final int validity_days = 365;

		final String agent_id = "p2_bat_cognito1";
		final String client_id = "uyv7rk6iz7878p4yx5raz727y95b4qm52a46vh222z458t8x9z";
		final String client_secret = "5996j6qy2a30ifg16o6fu7s2mupl38k602n8g8s37i4d8soxzq";
		/*-
		final String agent_id = "Omar.Gulam";
		final String client_id = "s09q461v6rzg48gw86kq3nl9thks7ssg8ag80710n1bms51gs9";
		final String client_secret = "47864y354004698r9j82c54o654d8b0lmqpd9k14jj5dh03t3a";
		*/
		try {
			createClientCertTest(client_id, client_secret, agent_id, validity_days);

			validateSampleSignature();
		} catch (NoSuchAlgorithmException | OperatorCreationException | CertificateException | IOException
				| InvalidKeyException | SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// FIXME Need to consider how this integrates with the Mule runtime...
	public static synchronized void initialiseKeyStore(String keystoreFilename, String keystorePassword, String caAlias,
			String caKeyPassword) {
		if (KEY_STORE == null) {
			try (InputStream is = KeystoreTest.class.getResourceAsStream(keystoreFilename)) {
				if (is == null) {
					System.out.println("Can't find keystore '" + keystoreFilename + "'");
					LOGGER.warn("Can't find keystore '{}'", keystoreFilename);
				} else {
					KEY_STORE = KeyStore.getInstance("PKCS12");

					System.out.println("Loading keystore '" + keystoreFilename + "'...");
					LOGGER.info("Loading keystore '" + keystoreFilename + "'...");
					KEY_STORE.load(is, keystorePassword.toCharArray());
					System.out.println("Loaded keystore '" + keystoreFilename + "'.");
					LOGGER.info("Loaded keystore '" + keystoreFilename + "'.");

					CA_PRIVATE_KEY = (PrivateKey) KEY_STORE.getKey(caAlias, caKeyPassword.toCharArray());
					CA_CERTIFICATE = (X509Certificate) KEY_STORE.getCertificate(caAlias);
				}
			} catch (Exception e) {
				KEY_STORE = null;
			}
		}
	}

	public static boolean isCertificateAuthorityPresent(String caAlias, String caKeyPassword) {
		try {
			return KEY_STORE.containsAlias(caAlias);
		} catch (Exception e) {
			LOGGER.error("Error", e);
			return false;
		}
	}

	public static ClientCert createClientCert(String agentId, int validityDays) throws NoSuchAlgorithmException,
			OperatorCreationException, CertificateException, IOException, InvalidKeyException, SignatureException {
		return createClientCertInternal(CA_PRIVATE_KEY, CA_CERTIFICATE, agentId, validityDays);
	}

	private static ClientCert createClientCertInternal(PrivateKey caKey, X509Certificate caCert, String agentId,
			int validityDays) throws NoSuchAlgorithmException, OperatorCreationException, CertificateException,
			IOException, InvalidKeyException, SignatureException {
		Instant created_at = Instant.now();

		// var created_at_timestamp = moment().utc().format("YYYY-MM-DDTHH:mm:ss[Z]")
		String created_at_timestamp = ZonedDateTime.ofInstant(created_at, ZoneOffset.UTC)
				.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME);

		KeyPairGenerator key_pair_gen = KeyPairGenerator.getInstance(RSA_ALGORITHM);
		SecureRandom random = SecureRandom.getInstanceStrong();
		key_pair_gen.initialize(AGENT_CERT_KEY_SIZE, random);

		KeyPair agent_key_pair = key_pair_gen.generateKeyPair();

		X500Name issuer = new X500Name(caCert.getIssuerDN().getName());
		BigInteger serial = BigInteger.valueOf(random.nextLong());
		Date not_before = Date.from(created_at);
		Date not_after = Date.from(created_at.plus(validityDays, ChronoUnit.DAYS));
		X500Name subject = new X500Name("CN=" + agentId);
		SubjectPublicKeyInfo pub_key_info = SubjectPublicKeyInfo.getInstance(agent_key_pair.getPublic().getEncoded());

		X509v3CertificateBuilder cert_builder = new X509v3CertificateBuilder(issuer, serial, not_before, not_after,
				subject, pub_key_info);
		ContentSigner signer = new JcaContentSignerBuilder(SIGNATURE_ALGORITHM).build(caKey);
		X509Certificate agent_cert = new JcaX509CertificateConverter().getCertificate(cert_builder.build(signer));

		String agent_public_key_pem;
		try (StringWriter writer = new StringWriter(); JcaPEMWriter pem_writer = new JcaPEMWriter(writer)) {
			pem_writer.writeObject(agent_cert);
			// Flush is required otherwise writer.toString() returns an empty string
			pem_writer.flush();

			agent_public_key_pem = writer.toString();
		}

		// Sqills uses a SHA-256 hash of the created_at timstamp
		// JavaScript Forge defaults to RSASSA PKCS#1 v1.5
		// This equates to SHA256WithRSA in Java

		// SHA256 of the timestamp
		// var md = forge.md.sha256.create();

		// Sign the timestamp
		// var signature = keys.privateKey.sign(md);
		Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
		sig.initSign(agent_key_pair.getPrivate());
		sig.update(created_at_timestamp.getBytes(StandardCharsets.UTF_8));
		String sig_b64 = new String(Base64.getEncoder().encode(sig.sign()), StandardCharsets.UTF_8);
		// pm.variables.set("iampoc_signature", forge.util.encode64(signature));

		return new ClientCert(created_at_timestamp, sig_b64, agent_public_key_pem.replace("\n", "\\n"));
	}

	public static void createClientCertTest(String clientId, String clientSecret, String agentId, int validityDays)
			throws NoSuchAlgorithmException, OperatorCreationException, CertificateException, IOException,
			InvalidKeyException, SignatureException {
		ClientCert client_cert = createClientCert(agentId, validityDays);

		System.out.println("{");
		System.out.format("\t\"grant_type\": \"%s\",%n", AGENT_GRANT_TYPE);
		System.out.format("\t\"client_id\": \"%s\",%n", clientId);
		System.out.format("\t\"client_secret\": \"%s\",%n", clientSecret);
		System.out.format("\t\"created_at\": \"%s\",%n", client_cert.getCreatedAt());
		System.out.format("\t\"signature\": \"%s\",%n", client_cert.getSignatureB64());
		System.out.println("\t\"authentication_group_code\": \"\",");
		System.out.format("\t\"public_key\": \"%s\"%n", client_cert.getPublicKeyPem());
		System.out.println("}");
		/*-
		 * Example:
		 * {
		 *  "grant_type": "https://com.sqills.s3.oauth.agent",
		 *  "client_id": "s09q461v6rzg48gw86kq3nl9thks7ssg8ag80710n1bms51gs9",
		 *  "client_secret": "47864y354004698r9j82c54o654d8b0lmqpd9k14jj5dh03t3a",
		 *  "created_at": "2021-05-18T15:32:29Z",
		 *  "signature": "MxlcoG9EN+uSXREtm0pumOVjpUvEP/+ocOZeOKZfKqvPtkgWCjSKA/wHhXTrACkCf7iJ8l1RLwjiUJxSQRECT6Wm7Q8OLNC9TKowN7mkM8cGU7N6C9MoT7FNT8K45/2xT5f4jxHrxiuqtfu+vidbt+U5E8CfpFFdyTyn527z8Ak=",
		 *  "authentication_group_code": "",
		 *  "public_key": "-----BEGIN CERTIFICATE-----\nMIIDxzCCAa+gAwIBAgIBATANBgkqhkiG9w0BAQsFADCBvDELMAkGA1UEBhMCVUsx\nEDAOBgNVBAgMB0VuZ2xhbmQxDzANBgNVBAcMBkxvbmRvbjEMMAoGA1UECgwDUkRH\nMQwwCgYDVQQLDANSREcxPDA6BgNVBAMMM3JkZy1tdWxlc29mdC1wdWJsaWMtY2Vy\ndC5zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbTEwMC4GCSqGSIb3DQEJARYhcmRn\nbXVsZWNlcnRAcmFpbGRlbGl2ZXJ5Z3JvdXAuY29tMB4XDTIxMDUxODE1MjExNloX\nDTIxMDUxODE3MjMxNlowFTETMBEGA1UEAxMKT21hci5HdWxhbTCBnzANBgkqhkiG\n9w0BAQEFAAOBjQAwgYkCgYEAq2Id77tvNIgWQCuHkljaEO2RxSmmLJcWGxrR9xl1\nd1GFHHR4f5YCwYSOodjiCro9YVo/MzzJxF+/5mN01ErKdCDjt9YfG7h0Wi6+al0N\nx2xeqH8WTVV4rdAxuSLkMlkc6eq1+J3Off9uBfD0JfarlpJ3sut08En9/LWMbmMg\ncmUCAwEAATANBgkqhkiG9w0BAQsFAAOCAgEAEz1uIeu32luOh6aoLn4iilx6tLkq\nsnXO+9+IeMXB12q4TrA645kXczT4mGgesu8zgyJzrDbbcoEUaoNvonKHnnsWYp3q\nbR96v9AHbDDtvJCVXQGcyigO/ilhMnzDnqNnA9uu4EyoBRLprxMdbfeT44p0pENJ\nDnoafcUW9lNi0ajwfuXStfoQ0LU+D2pWW22sJrICJbFxOZBJDic+wIZAGMrXTtXk\nseJXd/4tu7g+GX9WrW8EZXqhXeTdAkDxmU9WL/Bolh4H6tTz3HDZ2Jei9/IaL9iY\n5Nc97XQOiSHHnIewO6ab8b8aBZm8PlwoxUoFyaVxQns0euOI03Tn3N5JX2ZGhno+\n790QZFZEaJlWoajB3+dxrfr1a0R9C8DdZz5KgilB5sftMxjGxtVp8sHEEvHTxCPH\nLtomKusJZ6KmfMILqozmj6PFi5VQ3D2dFB7xyMHeiBCDnnLdsCTV+tkNVqtRpLXT\nYxal9XP2Q88f40bJBKX0nb/c4CnQKtXll9O0lYDAFSdAFA6bScTcapAxtz+DKZjz\nUKcnWHW0c1E7VVY/ZxWq51zOmfaD7qiNNklh9qDnoJmCXWYfEcZTNi1u4KrpBO4I\nN5HGtXgXjQ9gUE4Xm3X8A0Y6mPl0rWQBgMHptpVvqnGkajoVQE6XZkygLPSPGqxJ\nL1HkkeF+eCPhluU=\n-----END CERTIFICATE-----\n"
		 * }
		 */
	}

	public static void validateSampleSignature() throws CertificateException, IOException, NoSuchAlgorithmException,
			SignatureException, InvalidKeyException {
		/*-
		 * Captured from Postman (Agent Id = "Omar.Gulam"):
		 * {
		 *     "grant_type": "https://com.sqills.s3.oauth.agent",
		 *     "client_id": "s09q461v6rzg48gw86kq3nl9thks7ssg8ag80710n1bms51gs9",
		 *     "client_secret": "47864y354004698r9j82c54o654d8b0lmqpd9k14jj5dh03t3a",
		 *     "created_at": "2021-05-18T16:48:02Z",
		 *     "signature": "Iwgawl9kqBvJ7GzMFEwMvdONXyayWGCU67sLsl+yBEyBo883+U+cNC1VCTkOKlro+UpF0DUUshLoMKVlM74jYtMqEo5wHgBiSbXBmZ5FygWK7F+mOS1qwiPrWRZGmHCl3o6AkVBM9Vaam/w8QVQct99NRhgjKXqeIZ9lCMhNGuQ=",
		 *     "authentication_group_code": "",
		 *     "public_key": "-----BEGIN CERTIFICATE-----\nMIIDxzCCAa+gAwIBAgIBATANBgkqhkiG9w0BAQsFADCBvDELMAkGA1UEBhMCVUsx\nEDAOBgNVBAgMB0VuZ2xhbmQxDzANBgNVBAcMBkxvbmRvbjEMMAoGA1UECgwDUkRH\nMQwwCgYDVQQLDANSREcxPDA6BgNVBAMMM3JkZy1tdWxlc29mdC1wdWJsaWMtY2Vy\ndC5zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbTEwMC4GCSqGSIb3DQEJARYhcmRn\nbXVsZWNlcnRAcmFpbGRlbGl2ZXJ5Z3JvdXAuY29tMB4XDTIxMDUxODE1NDcwM1oX\nDTIxMDUxODE3NDkwM1owFTETMBEGA1UEAxMKT21hci5HdWxhbTCBnzANBgkqhkiG\n9w0BAQEFAAOBjQAwgYkCgYEAiM1eKN0G9hf0U3JKkpZxqFQceU0Xlq7nhHlbnhim\nElly8Gyc9eJblyh+2RSyooaLDH77tCiPtSRoOyQTkU3iOiLV9+No/Cc+WxBI3ZDS\nrqL3jlG3yTAOMVY3ZVENGSgS5q2GYW1OnqAba3IV2xTJ0D22EfVxWjLKSIAFpJYd\nj6UCAwEAATANBgkqhkiG9w0BAQsFAAOCAgEAQ2uQmk4fZrGGeMekfQEQEGDQsayH\n58gZBlA93m3WULkjsjlrMy1TH9t63Uh7V+zY7nOq3dO1xKnDgmvnnaldCyXQ8EgW\nhtwYp4LCz5CHMgBVlyaht5xjIrQBDERK9TO/cacPv49a+rwntajqva7h5GqTTJAA\n2yF3nze1Li06tbYZli3QVZ0vG4XbFbPrP/3fqT1m3CuRjbV1CL2RzIvKf5HXZ+7R\no9wHo++7HkeKUR4Su8at8oQp6R+Oj/y7c7Se8G1DYDDYIYZJ4sbT9LJEnnV0uXB3\nW2QVaaX1z6l7wevWIL2ql4BnXXY80pdTs9e6mSG2LgDZwPxSpaWgKi2K8kOGZdmw\ny97mIC+ryyee7iW+DIZs/PHeN9ghmR0cIln3jjJ/4ThDrlGM6SSWt3dLJ1f2pVEg\nQ0gnxsHNt6j58Dw0XAVxf5cyYqdBTZo9xeiL7b+FkXsJBJUYoUDwsgs0ShCbZGOe\nRleZyhsaMu1IsmkXY1WzaMuyphWAIUgLq73o0UDEGfOiT5tbwEbx0+vsO+su/FV1\nwQHrm4asXrn4mEviEeRao4IgV4hrHf6TEzF8ulnAwUwRnjjvznagGOoeATNGD8ps\nHi47L4D7qxJtBcbV40nb50U15A81mQneedNumsu42LbP+FwZ7A3sVVknkkaCTJFi\nw5M6c7wXwF/oCU8=\n-----END CERTIFICATE-----\n"
		 * }
		 */
		String created_at_timestamp = "2021-05-18T16:48:02Z";
		String signature_b64 = "Iwgawl9kqBvJ7GzMFEwMvdONXyayWGCU67sLsl+yBEyBo883+U+cNC1VCTkOKlro+UpF0DUUshLoMKVlM74jYtMqEo5wHgBiSbXBmZ5FygWK7F+mOS1qwiPrWRZGmHCl3o6AkVBM9Vaam/w8QVQct99NRhgjKXqeIZ9lCMhNGuQ=";
		byte[] signature = Base64.getDecoder().decode(signature_b64);
		String public_key_pem = "-----BEGIN CERTIFICATE-----\nMIIDxzCCAa+gAwIBAgIBATANBgkqhkiG9w0BAQsFADCBvDELMAkGA1UEBhMCVUsx\nEDAOBgNVBAgMB0VuZ2xhbmQxDzANBgNVBAcMBkxvbmRvbjEMMAoGA1UECgwDUkRH\nMQwwCgYDVQQLDANSREcxPDA6BgNVBAMMM3JkZy1tdWxlc29mdC1wdWJsaWMtY2Vy\ndC5zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbTEwMC4GCSqGSIb3DQEJARYhcmRn\nbXVsZWNlcnRAcmFpbGRlbGl2ZXJ5Z3JvdXAuY29tMB4XDTIxMDUxODE1NDcwM1oX\nDTIxMDUxODE3NDkwM1owFTETMBEGA1UEAxMKT21hci5HdWxhbTCBnzANBgkqhkiG\n9w0BAQEFAAOBjQAwgYkCgYEAiM1eKN0G9hf0U3JKkpZxqFQceU0Xlq7nhHlbnhim\nElly8Gyc9eJblyh+2RSyooaLDH77tCiPtSRoOyQTkU3iOiLV9+No/Cc+WxBI3ZDS\nrqL3jlG3yTAOMVY3ZVENGSgS5q2GYW1OnqAba3IV2xTJ0D22EfVxWjLKSIAFpJYd\nj6UCAwEAATANBgkqhkiG9w0BAQsFAAOCAgEAQ2uQmk4fZrGGeMekfQEQEGDQsayH\n58gZBlA93m3WULkjsjlrMy1TH9t63Uh7V+zY7nOq3dO1xKnDgmvnnaldCyXQ8EgW\nhtwYp4LCz5CHMgBVlyaht5xjIrQBDERK9TO/cacPv49a+rwntajqva7h5GqTTJAA\n2yF3nze1Li06tbYZli3QVZ0vG4XbFbPrP/3fqT1m3CuRjbV1CL2RzIvKf5HXZ+7R\no9wHo++7HkeKUR4Su8at8oQp6R+Oj/y7c7Se8G1DYDDYIYZJ4sbT9LJEnnV0uXB3\nW2QVaaX1z6l7wevWIL2ql4BnXXY80pdTs9e6mSG2LgDZwPxSpaWgKi2K8kOGZdmw\ny97mIC+ryyee7iW+DIZs/PHeN9ghmR0cIln3jjJ/4ThDrlGM6SSWt3dLJ1f2pVEg\nQ0gnxsHNt6j58Dw0XAVxf5cyYqdBTZo9xeiL7b+FkXsJBJUYoUDwsgs0ShCbZGOe\nRleZyhsaMu1IsmkXY1WzaMuyphWAIUgLq73o0UDEGfOiT5tbwEbx0+vsO+su/FV1\nwQHrm4asXrn4mEviEeRao4IgV4hrHf6TEzF8ulnAwUwRnjjvznagGOoeATNGD8ps\nHi47L4D7qxJtBcbV40nb50U15A81mQneedNumsu42LbP+FwZ7A3sVVknkkaCTJFi\nw5M6c7wXwF/oCU8=\n-----END CERTIFICATE-----\n";

		CertificateFactory fact = CertificateFactory.getInstance("X.509");
		try (ByteArrayInputStream bais = new ByteArrayInputStream(public_key_pem.getBytes())) {
			X509Certificate public_key_cert = (X509Certificate) fact.generateCertificate(bais);
			PublicKey public_key = public_key_cert.getPublicKey();

			// Verify the timestamp signature
			// Sqills use a SHA-256 hash of the created_at timstamp
			// JavaScript Forge defaults to RSASSA PKCS#1 v1.5
			// This equates to SHA256WithRSA in Java
			Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
			sig.initVerify(public_key);
			// No need to do message digest in Java - that is handled by the signature
			// algorithm
			// var signature = keys.privateKey.sign(md);
			sig.update(created_at_timestamp.getBytes(StandardCharsets.UTF_8));

			System.out.println("verified? " + sig.verify(signature));
		}
	}

	public static final class ClientCert {
		private String createdAt;
		private String signatureB64;
		private String publicKeyPem;

		public ClientCert(String createdAt, String signatureB64, String publicKeyPem) {
			this.createdAt = createdAt;
			this.signatureB64 = signatureB64;
			this.publicKeyPem = publicKeyPem;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public String getSignatureB64() {
			return signatureB64;
		}

		public String getPublicKeyPem() {
			return publicKeyPem;
		}
	}
}
