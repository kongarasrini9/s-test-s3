```
openssl pkcs12 -export -in iampoc_root.crt -inkey iampoc_root.key -out keystore.p12 -name rdg-dev-certauth-ca -caname rdg-dev-certauth-ca

keytool -list -keystore keystore.p12 -storepass rdg-dev
```
